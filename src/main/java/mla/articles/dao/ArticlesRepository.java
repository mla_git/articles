package mla.articles.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import mla.articles.model.Article;

@Repository
public interface ArticlesRepository extends JpaRepository<Article, Long> {
	@Query("SELECT DISTINCT source FROM Article where source is not null and source <> 'null' order by source")
	List<String> findSources();
	
	@Query("SELECT DISTINCT edition FROM Article where edition is not null and edition <> 'null' order by edition")
	List<String> findEditions();
	
	@Query("SELECT DISTINCT departements FROM Article where departements is not null and departements <> 'null' order by departements")
	List<String> findDepartements();
	
	@Query("SELECT DISTINCT regions FROM Article where regions is not null and regions <> 'null' order by regions")
	List<String> findRegions();
	
	@Query("SELECT DISTINCT secteurs FROM Article where secteurs is not null and secteurs <> 'null' order by secteurs")
	List<String> findSecteurs();
	
	@Query("SELECT DISTINCT themes FROM Article where themes is not null and themes <> 'null' order by themes")
	List<String> findThemes();
}
