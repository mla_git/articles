package mla.articles.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import mla.articles.model.Article;

@Configuration
public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter   {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Article.class);
    }
}
