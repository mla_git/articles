package mla.articles.dao;

import java.util.List;

import mla.articles.model.Article;

public interface ArticlesService {

	public List<Article> getAllArticles();
	
	public List<Article> findArticlesByTitle(String title);

	public void saveAll(List<Article> listArticles);

	public Article findArticlesById(Long id);

	public List<String> getSources();
	
	public List<String> getDepartements();
	
	public List<String> getRegions();
	
	public List<String> getSecteurs();
	
	public List<String> getThemes();
	
	public List<String> getEditions();

	public void save(Article article);
}
