package mla.articles.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import mla.articles.model.Article;

@Service
public class ArticlesServiceImpl implements ArticlesService {

	private ArticlesRepository repository;

	@Autowired
	public ArticlesServiceImpl(ArticlesRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public List<Article> getAllArticles() {
		return repository.findAll(Sort.by(Direction.DESC,"publicationDate"));
	}

	@Override
	public List<Article> findArticlesByTitle(String title) {
		Article article = new Article();
		article.setTitre(title);
		return repository.findAll(Example.of(article));
	}

	@Override
	public void saveAll(List<Article> articles) {
		repository.saveAll(articles);
	}

	@Override
	public Article findArticlesById(Long id) {
		return repository.findById(id).get();
	}

	@Override
	public List<String> getSources() {
		return repository.findSources();
	}

	@Override
	public List<String> getDepartements() {
		List<String> departements = repository.findDepartements();
		return getUniqueValues(departements);
	}

	@Override
	public List<String> getRegions() {
		List<String> regions = repository.findRegions();
		return getUniqueValues(regions);
	}


	@Override
	public List<String> getSecteurs() {
		List<String> secteurs = repository.findSecteurs();
		return getUniqueValues(secteurs);
	}

	@Override
	public List<String> getThemes() {
		List<String> themes = repository.findThemes();
		return getUniqueValues(themes);
	}

	@Override
	public List<String> getEditions() {
		return repository.findEditions();
	}

	/**
	 * Method to get unique values (from multi value field)
	 * @param values
	 * @return
	 */
	private List<String> getUniqueValues(List<String> values) {
		List<String> allValues = new ArrayList<String>();
		for(String value : values) {
			String[] vals = value.split(",");
			allValues.addAll(Arrays.asList(vals));
		}
		
		List<String> result = allValues.stream().distinct().collect(Collectors.toList());
		Collections.sort(result);
		return result;
	}

	@Override
	public void save(Article article) {
		repository.saveAndFlush(article);
	}
}
