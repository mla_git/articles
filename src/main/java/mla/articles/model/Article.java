package mla.articles.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Marc
 *
 *         Class representing an article
 *
 */
@Entity
public class Article {

	@Id
	@GeneratedValue
	private Long id;

	@Column(length = 500)
	private String titre;

	@Column(length = 5000)
	private String corps;

	@Column
	@SerializedName("publication_date")
	private Date publicationDate;

	@Column
	private String source;

	@Column
	private String edition;

	@Column
	private String departements;

	@Column
	private String regions;

	@Column
	private String secteurs;

	@Column
	private String themes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getDepartements() {
		return departements;
	}

	public void setDepartements(String departements) {
		this.departements = departements;
	}

	public String getRegions() {
		return regions;
	}

	public void setRegions(String regions) {
		this.regions = regions;
	}

	public String getSecteurs() {
		return secteurs;
	}

	public void setSecteurs(String secteurs) {
		this.secteurs = secteurs;
	}

	public String getThemes() {
		return themes;
	}

	public void setThemes(String themes) {
		this.themes = themes;
	}

}
