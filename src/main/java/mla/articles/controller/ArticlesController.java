package mla.articles.controller;

import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mla.articles.dao.ArticlesService;
import mla.articles.model.Article;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ArticlesController {
	
	@Autowired
	ArticlesService service;
	
	@RequestMapping("/articles")
	public List<Article> getArticles() {
		List<Article> articles = service.getAllArticles();
		return articles;
	}
	
	@RequestMapping("/articleById/{id}")
	public Article getArticleById(@PathVariable("id") Long id) {
		Article article = service.findArticlesById(id);
		return article;
	}
	
	@RequestMapping("/sources")
	public List<String> getSources() {
		return service.getSources();
	}
	
	@RequestMapping("/editions")
	public List<String> getEditions() {
		return service.getEditions();
	}
	
	@RequestMapping("/regions")
	public List<String> getRegions() {
		return service.getRegions();
	}
	
	@RequestMapping("/departements")
	public List<String> getDepartements() {
		return service.getDepartements();
	}
	
	@RequestMapping("/themes")
	public List<String> getThemes() {
		return service.getThemes();
	}
	
	@RequestMapping("/secteurs")
	public List<String> getSecteurs() {
		return service.getSecteurs();
	}
	
    @PostMapping("/addArticle")
    void addArticle(@RequestBody Article article) {
    	article.setPublicationDate(new Date());
        service.save(article);
    }
	
	@RequestMapping("/initDatabase")
	public String initDatabase() {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create();
		
		Article[] articles;
		try (Reader reader = new FileReader("src\\main\\resources\\database\\site_articles.json")) {
			articles = gson.fromJson(reader, Article[].class);
			List<Article> listArticles = Arrays.asList(articles);
			service.saveAll(listArticles);
		} catch (Exception e) {
			e.printStackTrace();
			return "An error occurred : " + e.getMessage();
		}
		
		
		
		return "success";
	}
}
